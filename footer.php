<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package BootOver
 */
?>

<?php get_sidebar('footerfull'); ?>

<div class="wrapper" id="wrapper-footer">
    
    <footer class="container-fluid navbar-inverse no-pad">

        <!--<div class="row">-->

            <div class="container">
    
                <footer id="colophon" class="site-footer" role="contentinfo">

                    <div class="site-info pull-right">
                        <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'BootOver' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'BootOver' ), 'WordPress' ); ?></a>
                        <span class="sep"> | </span>
                        <?php printf( __( 'Theme: %1$s by %2$s.', 'BootOver' ), 'BootOver', '<a href="https://www.cryptocodez.de/" rel="designer">CryptoCodez</a>' ); ?> 
                        (<?php printf( __( 'Version', 'BootOver' ) ); ?>: 0.0.9)
                    </div><!-- .site-info -->

                </footer><!-- #colophon -->

            </div><!--col end -->

        <!-- </div> --><!-- row end -->
        
    </footer><!-- container end -->
    
</div><!-- wrapper end -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>

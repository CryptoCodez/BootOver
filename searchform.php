<?php
/**
 * The template for displaying search forms in Underscores.me
 *
 * @package BootOver
 */
?>
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
		<div class="row">	
			<div class="col-xs-12 col-sm-8 searchbox"><input type="text" class="field form-control" name="s" id="s" placeholder="<?php esc_attr_e( 'Search &hellip;', 'BootOver' ); ?>" ></div>
			<div class="col-xs-12 col-sm-4 searchbox"><input type="submit" class="submit btn btn-primary" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'BootOver' ); ?>" ></div>
		</div>
	</form>

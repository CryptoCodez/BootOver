## About

I’m a huge fan of Underscores, Bootstrap, and Sass. Why not combine these into a solid WordPress Theme Framework?
That’s what UnderStrap is. ( A Very Basic )
BootOver is based on it... ( just a few changes )

# BootOver WordPress Theme Framework

Website: [https://cryptocodez.de/](cryptocodez.de)

## License
BootOver is released under the terms of the GPL version 2 or (at your option) any later version.

http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

## Basic Features

- Combines Underscore’s PHP/JS files and Bootstrap’s HTML/CSS/JS.
- Comes with Bootstrap (v4) Sass source files and additional .scss files. Nicely sorted and ready to add your own variables and customize the Bootstrap variables.
- Uses a single and minified CSS file for all the basic stuff.
- [Font Awesome](http://fortawesome.github.io/Font-Awesome/) integration (v4.6.0)
- Comes with extra slider script by [Owl Carousel](http://www.owlcarousel.owlgraphic.com/) (v2.0.0-beta.2.4)
- Simple RTL file.
- Jetpack ready.
- WooCommerce support.
- [Child Theme](https://github.com/holger1411/understrap-child) ready.
- Translation ready.

